package com.java.Exercise48;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    private SongList songs;

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        songs = new SongList();
    }

    /**
     * add Song
     * @param title
     * @param duration
     * @return
     */
    public boolean addSong(String title, double duration){
        if (this.songs.findSong(title) == null){
            songs.songs.add(new Song(title,duration));
            System.out.println("Add success");
            return true;
        }
        return false;
    }

    public static class SongList{
        private ArrayList<Song> songs;

        private SongList() {
            this.songs = new ArrayList<>();
        }

        /**
         * add Song
         * @param s
         * @return true or false
         */
        private boolean addSong(Song s){
            if (this.findSong(s.getTitle()) == null){
                this.songs.add(s);
                return true;
            }
            return false;
        }

        /**
         * find Song
         * @param title
         * @return
         */
        private Song findSong(String title){
            for(Song s : songs){
                if(s.getTitle().equals(title)){
                    return s;
                }
            }
            return null;
        }

        /**
         * find Song
         * @param trackNumber
         * @return null or Song
         */
        private Song findSong(int trackNumber){
            if(trackNumber <1 || trackNumber > songs.size()){
                return null;
            }
            return songs.get(trackNumber-1);
        }

    }

}
