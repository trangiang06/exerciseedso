package com.java.ExampleDay3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Example {

    static int n = 0;
    static Double[] arrayDouble;

    /**
     * @param n
     */
    public static void functionOne(int n) {
        arrayDouble = new Double[n];
        for (int i = 0; i < arrayDouble.length; i++) {
            arrayDouble[i] = (Math.random()) * ((99 - 0 + 1)) + 0;
            System.out.println("Phần tử thứ " + i + " trong mảng là: " + arrayDouble[i]);
        }
    }

    /**
     * @param array
     */
    public static void functionTwo(Double[] array) {
        List list = new ArrayList();
        list.addAll(Arrays.asList(array));
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 50) {
                System.out.println("Phần tử lần lượt bị xóa nhỏ hơn 50 là : " + array[i]);
                list.remove(array[i]);
            }
            System.out.println("Phần tử mảng hiện tại là : " + list);
        }
        if (list.isEmpty()) {
            System.out.println("Phần tử trong mảng là rỗng");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số phần tử n của mảng : ");
        n = scanner.nextInt();
        functionOne(n);
        functionTwo(arrayDouble);

    }
}
