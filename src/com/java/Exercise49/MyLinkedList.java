package com.java.Exercise49;

public class MyLinkedList implements NodeList{
    private ListItem root = null;

    public MyLinkedList(ListItem root) {
        this.root = root;
    }

    @Override
    public ListItem getRoot() {
        return this.root;
    }

    @Override
    public boolean addItem(ListItem item) {
        if (this.root == null) {
            this.root = item;
            return true;
        }
        ListItem currentItem = this.root;
        while (currentItem != null) {
            int comparetion = (currentItem.compareTo(item));
            if (comparetion < 0) {
                if (currentItem.next() != null) {
                    currentItem = currentItem.next();
                } else {
                    currentItem.setNext(item);
                    item.setPrevious(currentItem);
                    return true;
                }
            } else if (comparetion > 0) {
                if (currentItem.previous() != null) {
                    currentItem.previous().setPrevious(item);
                    item.setPrevious((currentItem.previous()));
                    item.setNext(currentItem);
                    currentItem.setPrevious(item);
                } else {
                    item.setNext(this.root);
                    this.root.setPrevious(item);
                    this.root = item;
                }
                return true;
            } else {
                System.out.println(item.getValue() + " already exists, can not add.");
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean removeItem(ListItem item) {
        if(item != null) {
            System.out.println(" Delete item " + item.getValue());
        }
        ListItem currentItem = this.root;
        while ( currentItem != null){
            int compa = currentItem.compareTo(item);
            if(compa == 0){
                if(currentItem == this.root){
                    this.root = currentItem.next();
                } else  {
                    currentItem.previous().setNext(currentItem.next());
                    if(currentItem.next() != null){
                        currentItem.next().setPrevious(currentItem.previous());

                    }
                }
                return true;
            } else if (compa < 0){
                currentItem = currentItem.next();
            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    public void traverse(ListItem item) {
        if (root == null){
            System.out.println("is empty");
        } else {
            while (root != null) {
                System.out.println(root.getValue());
                root = root.next();
            }
        }
    }
}
