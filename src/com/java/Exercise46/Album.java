package com.java.Exercise46;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Album {
    private String name;
    private String artist;
    private List<Song> songs = new ArrayList();

    static Scanner scanner = new Scanner(System.in);
    static List<Album> albums = new ArrayList<>();
    static Album album;
    static LinkedList<Song> playList = new LinkedList<>();

    public Album() {
    }

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                ", artist='" + artist + '\'' +
                ", songs=" + songs +
                '}';
    }

    public static void main(String[] args) {

        album = new Album("Stormbringer", "Deep Purp");

        System.out.println("Input Song");
        System.out.println("Input title");
        String title = scanner.nextLine();
        System.out.println("Input duration");
        double duration = scanner.nextDouble();
        scanner.nextLine();

        album.addSong("Stormbringer", 4.6);
        album.addSong("Love don't mean a thing", 4.22);
        album.addSong("Holy man", 4.3);
        album.addSong("Hold on", 5.6);
        album.addSong("Lady double dealer", 3.21);
        album.addSong("You can't do it right", 6.23);
        album.addSong("High ball shooter", 4.27);
        album.addSong("The gypsy", 4.2);
        album.addSong("Soldier of fortune", 3.13);
        albums.add(album);

        album = new Album("For those about to rock", "AC/DC");
        album.addSong("For those about to rock", 5.44);
        album.addSong("I put the finger on you", 3.25);
        album.addSong("Lets go", 3.45);
        album.addSong("Inject the venom", 3.33);
        album.addSong("Snowballed", 4.51);
        album.addSong("Evil walks", 3.45);
        album.addSong("C.O.D.", 5.25);
        album.addSong("Breaking the rules", 5.32);
        album.addSong("Night of the long knives", 5.12);
        albums.add(album);

        if (album.addSong(title, duration) == true) {
            System.out.println("Add success");
        }
        ;
        for (Album album : albums) {
            System.out.println(album.getName() + album.getArtist() + album.getSongs());
        }

        System.out.println("Input numer of song in album");
        System.out.println("PlayList is ;");
        albums.get(0).addToPlayList(7, playList);
        albums.get(1).addToPlayList(3, playList);
        albums.get(1).addToPlayList(2, playList);
        albums.get(1).addToPlayList(32, playList);


    }

    /**
     * find song by title
     *
     * @param title
     * @return song or null
     */
    private static Song findSong(String title) {
        for (Album album : albums) {
            for (Song song : album.getSongs()) {
                if (song.getTitle().equals(title)) {
                    return song;
                }
            }
        }
        return null;
    }

    /**
     * add to play list
     *
     * @param numberOfSongs
     * @param playList
     * @return true or false
     */

    public boolean addToPlayList(int numberOfSongs, LinkedList<Song> playList) {

        for (Album album : albums) {

            for (int i = 0; i < numberOfSongs; i++) {
                if (numberOfSongs <= album.getSongs().size()) {
                    for (int j = 0; j < album.getSongs().size(); j++) {
                        playList.add(album.getSongs().get(j));
                        System.out.println(playList);
                        return true;
                    }
                }
            }
        }
        System.out.println("There is no track " + numberOfSongs);
        return false;
    }

    /**
     * add to play list
     *
     * @param titleOfSong
     * @param playList
     * @return true or false
     */
    public boolean addToPlayList(String titleOfSong, LinkedList<Song> playList) {

        for (Album album : albums) {

            for (Song song : album.getSongs()) {
                if (findSong(song.getTitle()).equals(titleOfSong)) {
                    playList.add(song);
                    return true;
                }
            }
        }
        System.out.println("Do not exist " + titleOfSong);
        return false;
    }

    /**
     * add Song
     *
     * @param title
     * @param duration
     * @return
     */
    public boolean addSong(String title, double duration) {
        Album album = new Album();
        Song song = new Song();
        song.setTitle(title);
        song.setDuration(duration);
        songs.add(song);
        album.setSongs(songs);
        if (songs.size() > 0) {
            return true;
        }
        return false;
    }
}
