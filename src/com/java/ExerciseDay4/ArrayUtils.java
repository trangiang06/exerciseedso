package com.java.ExerciseDay4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class ArrayUtils {
    private static ArrayUtils INSTANCE;


    private ArrayUtils() {
    }

    public static ArrayUtils getInstance() {
        return SingletonHelper.INSTANCE;
    }

    private static class SingletonHelper {
        private static final ArrayUtils INSTANCE = new ArrayUtils();
    }

    /**
     * display Object array
     *
     * @param array
     */
    public <T> void display(T[] array) {
        if (Objects.isNull(array) || array.length < 1) {
            System.out.println("Array invalid!");
        } else {
            for (int i = 0; i < array.length; i++) {
                System.out.println("Array is : " + array[i]);
            }
        }
    }
}
